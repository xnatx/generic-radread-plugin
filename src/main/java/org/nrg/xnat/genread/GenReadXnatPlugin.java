/*
 * xnat-gen-radread-plugin: org.nrg.xnat.genread.plugin.GenReadXnatPlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.genread;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.bean.RadGenradiologyreaddataBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
        value = "genreadPlugin",
        name = "XNAT Generic Read Plugin",
        dataModels = {
                @XnatDataModel(
                        value = RadGenradiologyreaddataBean.SCHEMA_ELEMENT_NAME,
                        singular = "Generic Radiology Read",
                        plural = "Generic Radiology Reads"
                )
        }
)
public class GenReadXnatPlugin {
    //  [*L*]  //
}
